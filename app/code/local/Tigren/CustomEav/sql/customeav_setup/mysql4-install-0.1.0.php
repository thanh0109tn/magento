<?php
$setup = new Mage_Eav_Model_Entity_Setup('catalog_setup');
$setup->startSetup();
$attr = array(
    'group' => 'General',
    'type' => 'text',
    'input' => 'text',
    'label' => 'Is Featured',
    'backend' => '',
    'frontend' => '',
    'source' => 'eav/entity_attribute_source_boolean',
    'visible' => 1,
    'user_defined' => 1,
    'used_for_price_rules' => 1,
    'position' => 2,
    'unique' => 0,
    'default' => '',
    'sort_order' => 101
    );
$setup->addAttribute('catalog_product','featured',$attr);
