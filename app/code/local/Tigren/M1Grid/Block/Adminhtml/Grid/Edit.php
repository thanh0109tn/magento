<?php

class Tigren_M1Grid_Block_Adminhtml_Grid_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'm1grid';
        $this->_controller = 'adminhtml_grid';
        $this->_updateButton('save', 'label','Save');
        $this->_updateButton('delete', 'label', 'Delete');
    }

    public function getHeaderText()
    {
        if( Mage::registry('grid_data') && Mage::registry('grid_data')->getId() )
        {
            return 'Edit '.$this->htmlEscape( Mage::registry('grid_data')->getTitle() );
        }
        else
        {
            return 'Add new';
        }
    }
}