<?php
class Tigren_M1Grid_Block_Adminhtml_Grid_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('grid_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Information');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => 'Information',
            'title' => 'Information',
            'content' => $this->getLayout()
                ->createBlock('m1grid/adminhtml_grid_edit_tab_form')
                ->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}