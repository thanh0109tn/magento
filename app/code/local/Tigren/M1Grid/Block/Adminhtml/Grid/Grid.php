<?php
class Tigren_M1Grid_Block_Adminhtml_Grid_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setDefaultSort('id');
        $this->setId('m1grid_grid_grid');
        $this->setDefaultDir('asc');
        $this->setSaveParametersInSession(true);
    }
    protected function _getCollectionClass()
    {
        return 'm1grid/grid_collection';
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn('id',
            array(
                'header'=> $this->__('ID'),
                'width' => '50px',
                'index' => 'id'
            )
        );
        $this->addColumn('name',
            array(
                'header'=> $this->__('Name'),
                'index' => 'name'
            )
        );
        $this->addColumn('testimonial',
            array(
                'header'=> $this->__('Testimonial'),
                'index' => 'testimonial'
            )
        );
        $this->addColumn('status',
            array(
                'header'=> $this->__('Status'),
                'index' => 'status'
            )
        );
        $this->addColumn('created',
            array(
                'header'=> $this->__('Created At'),
                'index' => 'created'
            )
        );
        $this->addColumn('modified',
            array(
                'header'=> $this->__('Modified At'),
                'index' => 'modified'
            )
        );
        return parent::_prepareColumns();
    }
    protected function _prepareMassaction()
    {
        $this->setMassactionIdField('id');
        $this->getMassactionBlock()->setFormFieldName('id');
        $this->getMassactionBlock()->addItem('delete', array(
            'label'    => Mage::helper('core')->__('Delete'),
            'url'      => $this->getUrl('*/*/massDelete'),
            'confirm'  => Mage::helper('core')->__('Are you sure?')
        ));

        return $this;
    }
    public function getRowUrl($row)
    {
        return $this->getUrl('*/*/edit', array('id' => $row->getId()));
    }
}