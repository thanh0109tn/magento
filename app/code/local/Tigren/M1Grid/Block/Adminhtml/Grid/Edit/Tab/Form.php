<?php
class Tigren_M1Grid_Block_Adminhtml_Grid_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('grid_form',
            array('legend'=>'information'));
        $fieldset->addField('name', 'text',
            array(
                'label' => 'Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'name',
            ));
        $fieldset->addField('testimonial', 'text',
            array(
                'label' => 'Testimonial',
                'name' => 'testimonial',
            ));


        if ( Mage::registry('grid_data') )
        {
            $form->setValues(Mage::registry('grid_data')->getData());
        }

        return parent::_prepareForm();
    }
}
