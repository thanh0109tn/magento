<?php
class Tigren_M1Grid_Block_Adminhtml_Grid extends Mage_Adminhtml_Block_Widget_Grid_Container {
    public function __construct()
    {
        $this->_blockGroup = 'm1grid';
        $this->_controller = 'adminhtml_grid';
        $this->_headerText = $this->__('Grid');
        $this->_addButton('add', array(
            'label'     => $this->_addButtonLabel = 'Add new record',
            'onclick'   => 'setLocation(\'' . $this->getUrl('*/*/new') .'\')',
            'class'     => 'add',
        ));

        parent::__construct();
    }
}