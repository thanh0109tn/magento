<?php
class Tigren_M1Grid_Model_Resource_Grid extends Mage_Core_Model_Resource_Db_Abstract
{
    protected function _construct()
    {
        $this->_init('m1grid/grid', 'id');
    }
}