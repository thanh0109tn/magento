<?php
class Tigren_M1Grid_IndexController extends Mage_Core_Controller_Front_Action
{
    public function indexAction()
    {
        $this->loadLayout();
        $this->renderLayout();
    }
    public function editAction()
    {
        $gridId = $this->getRequest()->getParam('id');
        $gridModel = Mage::getModel('m1grid/grid')->load($gridId);

        if ($gridModel->getId() || $gridId == 0)
        {
            Mage::register('grid_data', $gridModel);
            $this->loadLayout();
            $this->getLayout()->getBlock('head')
                ->setCanLoadExtJs(true);
            $this->_addContent($this->getLayout()
                ->createBlock('m1grid/adminhtml_grid_edit'))
                ->_addLeft($this->getLayout()
                    ->createBlock('m1grid/adminhtml_grid_edit_tabs')
                );
            $this->renderLayout();
        }
        else
        {
            Mage::getSingleton('adminhtml/session')->addError('Grid does not exist');
            $this->_redirect('*/*/');
        }
    }

    public function newAction()
    {
        $this->_forward('edit');
        echo 'abc';
    }

    public function saveAction()
    {
        if ($this->getRequest()->getPost())
        {
            try {
                $postData = $this->getRequest()->getPost();
                $gridModel = Mage::getModel('m1grid/grid');

                if( $this->getRequest()->getParam('id') <= 0 ) {
                    $gridModel->setCreatedTime( Mage::getSingleton('core/date')->gmtDate() );
                    $gridModel
                        ->addData($postData)
                        ->setUpdateTime( Mage::getSingleton('core/date')->gmtDate())
                        ->setId($this->getRequest()->getParam('id'))
                        ->save();

                    Mage::getSingleton('adminhtml/session')->addSuccess('successfully saved');
                    Mage::getSingleton('adminhtml/session')->setfilmsData(false);
                    $this->_redirect('*/*/');
                    return;
                }

            } catch (Exception $e) {

                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                Mage::getSingleton('adminhtml/session')->setfilmsData($this->getRequest()->getPost());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
                return;
            }

            $this->_redirect('*/*/');
        }
    }

    public function deleteAction()
    {
        if($this->getRequest()->getParam('id') > 0)
        {
            try
            {
                $gridModel = Mage::getModel('m1grid/grid');
                $gridModel->setId($this->getRequest()->getParam('id'))->delete();
                Mage::getSingleton('adminhtml/session')->addSuccess('successfully deleted');
                $this->_redirect('*/*/');
            }
            catch (Exception $e)
            {
                Mage::getSingleton('adminhtml/session')->addError($e->getMessage());
                $this->_redirect('*/*/edit', array('id' => $this->getRequest()->getParam('id')));
            }
        }
        $this->_redirect('*/*/');
    }
}