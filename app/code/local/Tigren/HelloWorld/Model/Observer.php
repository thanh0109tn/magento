<?php
class Tigren_HelloWorld_Model_Observer
{
    public function changePrice($observer) {
        $product = $observer->getEvent()->getProduct();
        $newPrice = $product->setPrice('200');
        return $newPrice;
    }
//    public function setPrice($observer)
//    {
//        $event = $observer->getEvent();
//        $product = $event->getProduct();
//        // process percentage discounts only for simple products
//        if ($product->getSuperProduct() && $product->getSuperProduct()->isConfigurable()) {
//        } else {
//
//            $product->setFinalPrice(10);
//        }
//        return $this;
//    }
}