<?php
require_once (Mage::getModuleDir('controllers','Mage_Checkout').DS.'CartController.php');
//require_once 'Mage/Checkout/controllers/CartController.php';
class Tigren_HelloWorld_CartController extends Mage_Checkout_CartController
{
    public function indexAction()
    {
        if( ! Mage::getSingleton('customer/session')->isLoggedIn()){
            $this->_redirect('customer/account/login/');
        }
    }
}