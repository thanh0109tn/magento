<?php
class Tigren_Crud_Adminhtml_TestController extends Mage_Adminhtml_Controller_Action
{
    protected function _isAllowed()
    {
        return Mage::getSingleton('admin/session')->isAllowed('crud/test');
    }
    public function indexAction()
    {
        $this->loadLayout();
        $this->_title($this->__("Crud"));
        $this->renderLayout();
    }
}