<?php
class Tigren_Crud_Block_Adminhtml_Test extends Mage_Adminhtml_Block_Widget_Grid_Container
{
    public function __construct()
    {
        $this->_controller = 'adminhtml_test';
        $this->_blockGroup = 'tigren_crud';
        $this->_headerText = 'Test';
        parent::__construct();
    }
}