<?php

class Tigren_Crud_Block_Adminhtml_Crud_Edit extends Mage_Adminhtml_Block_Widget_Form_Container
{
    public function __construct()
    {
        parent::__construct();
        $this->_objectId = 'id';
        $this->_blockGroup = 'tigren_crud';
        $this->_controller = 'adminhtml_crud';
        $this->_updateButton('save', 'label','Save');
        $this->_updateButton('delete', 'label', 'Delete');
    }

    public function getHeaderText()
    {
        if( Mage::registry('test_data') && Mage::registry('test_data')->getId() )
        {
            return 'Edit '.$this->htmlEscape( Mage::registry('test_data')->getTitle() );
        }
        else
        {
            return 'Add new';
        }
    }
}