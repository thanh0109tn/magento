<?php
class Tigren_Crud_Block_Adminhtml_Test_Grid extends Mage_Adminhtml_Block_Widget_Grid
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('crud_test_grid');
        $this->setDefaultSort('test_id');
        $this->setDefaultDir('DESC');
        $this->setSaveParametersInSession(true);
    }
    protected function _getCollectionClass()
    {
        return 'crud/test_collection';
    }
    protected function _prepareCollection()
    {
        $collection = Mage::getResourceModel($this->_getCollectionClass());
        $this->setCollection($collection);
        return parent::_prepareCollection();
    }
    protected function _prepareColumns()
    {
        $this->addColumn('test_id',
            array(
                'header' => 'ID',
                'align' =>'right',
                'width' => '50px',
                'index' => 'test_id',
            )
        );
        $this->addColumn('name',
            array(
                'header' => 'Name',
                'index' => 'name',
            )
        );
        return parent::_prepareColumns();
    }
}