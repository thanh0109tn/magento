<?php
class Tigren_Crud_Block_Adminhtml_Crud_Edit_Tabs extends Mage_Adminhtml_Block_Widget_Tabs
{
    public function __construct()
    {
        parent::__construct();
        $this->setId('crud_tabs');
        $this->setDestElementId('edit_form');
        $this->setTitle('Information');
    }

    protected function _beforeToHtml()
    {
        $this->addTab('form_section', array(
            'label' => 'Information',
            'title' => 'Information',
            'content' => $this->getLayout()
                ->createBlock('tigren_crud/adminhtml_crud_edit_tab_form')
                ->toHtml()
        ));

        return parent::_beforeToHtml();
    }
}