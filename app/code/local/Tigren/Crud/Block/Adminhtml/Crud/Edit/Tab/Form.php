<?php
class Tigren_Crud_Block_Adminhtml_Crud_Edit_Tab_Form extends Mage_Adminhtml_Block_Widget_Form
{
    protected function _prepareForm()
    {
        $form = new Varien_Data_Form();
        $this->setForm($form);
        $fieldset = $form->addFieldset('crud_form',
            array('legend'=>'information'));
        $fieldset->addField('name', 'text',
            array(
                'label' => 'Name',
                'class' => 'required-entry',
                'required' => true,
                'name' => 'name',
            ));

        if ( Mage::registry('test_data') )
        {
            $form->setValues(Mage::registry('test_data')->getData());
        }

        return parent::_prepareForm();
    }
}
